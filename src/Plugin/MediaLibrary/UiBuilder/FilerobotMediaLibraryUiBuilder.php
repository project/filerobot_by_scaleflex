<?php
namespace Drupal\filerobot\Plugin\MediaLibrary\UiBuilder;

use Drupal\media_library\MediaLibraryState;
use Drupal\media_library\MediaLibraryUiBuilder;

class FilerobotMediaLibraryUiBuilder extends MediaLibraryUiBuilder {
  /**
   * Build the media library UI.
   *
   * @param \Drupal\media_library\MediaLibraryState $state
   *   (optional) The current state of the media library, derived from the
   *   current request.
   *
   * @return array
   *   The render array for the media library.
   */
  public function buildUi(MediaLibraryState $state = NULL) {
    if (!$state) {
      $state = MediaLibraryState::fromRequest($this->request);
    }
    // When navigating to a media type through the vertical tabs, we only want
    // to load the changed library content. This is not only more efficient, but
    // also provides a more accessible user experience for screen readers.
    if ($state->get('media_library_content') === '1') {
      return $this->buildLibraryContent($state);
    } else {
      return [
        '#theme' => 'media_library_wrapper',
        '#attributes' => [
          'id' => 'media-library-wrapper',
        ],
        'menu' => $this->buildMediaTypeMenu($state),
        'content' => $this->buildLibraryContent($state),
        // Attach the JavaScript for the media library UI. The number of
        // available slots needs to be added to make sure users can't select
        // more items than allowed.
        '#attached' => [
          'library' => ['media_library/ui'],
          'drupalSettings' => [
            'media_library' => [
              'selection_remaining' => $state->getAvailableSlots(),
            ],
          ],
        ],
      ];
    }
  }

  protected function buildLibraryContent(MediaLibraryState $state) {
    $config = \Drupal::config('filerobot.admin_settings');
    $activation = $config->get('activation');
    $token = $config->get('token');
    if ($activation && $token != '') {
      if ($state->getSelectedTypeId() != 'remote_video') {
        $attr = [];
        $attr['id'] = 'filerobot-widget';
        $attr['style'] = 'min-width: 700px; min-height: 500px';
        if ($state->getOpenerId() == 'media_library.opener.editor') {
          $attr['class'] = 'open-editor';
        }
        return [
          '#type' => 'container',
          '#theme_wrappers' => [
            'container__media_library_content',
          ],
          '#attributes' => $attr,
          '#attached' => [
            'library' => ['filerobot/filerobot-js']
          ],
          'form' => '',
          'view' => '',
        ];
      }
    }
    return [
      '#type' => 'container',
      '#theme_wrappers' => [
        'container__media_library_content',
      ],
      '#attributes' => [
        'id' => 'media-library-content',
      ],
      'form' => $this->buildMediaTypeAddForm($state),
      'view' => $this->buildMediaLibraryView($state),
    ];
  }
}
