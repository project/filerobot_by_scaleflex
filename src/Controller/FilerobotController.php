<?php

namespace Drupal\filerobot\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\File\FileSystemInterface;
use Drupal\File\FileRepositoryInterface;
use Drupal\media\Entity\Media;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FilerobotController extends ControllerBase
{
  private $database;
  private $fileSystem;
  private $fileRepository;

  public function __construct(Connection $database, FileSystemInterface $file_system, FileRepositoryInterface $file_repository) {
    $this->database = $database;
    $this->fileSystem = $file_system;
    $this->fileRepository = $file_repository;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('file_system'),
      $container->get('file.repository')
    );
  }

  public function insertImage(Request $request) {
    $data = json_decode($request->getContent(), TRUE);

    $query = $this->database->select('filerobot_medias', 'fm');
    $query->fields('fm', ['uuid', 'media_id', 'url', 'filename', 'local_uuid'])
      ->condition('fm.uuid', $data['uuid']);
    $result = $query->execute()->fetchAll();

    if (!count($result)) {
      $url = $data['url'];

      // Download the image from the URL.
      $contents = file_get_contents($url);

      $baseUrl = explode('?', basename($url));
      $filename = $baseUrl[0];

      $dirname =  'public://filerobot';
      $this->fileSystem->prepareDirectory($dirname, FileSystemInterface::CREATE_DIRECTORY);

      $destination = $dirname . '/' . $filename;
      $file = $this->fileRepository->writeData($contents, $destination, FileSystemInterface::EXISTS_REPLACE);

      // Create a new media entity.
      $media = Media::create([
        'bundle' => 'image',
        'name' => $baseUrl[0],
        'field_media_image' => [
          'target_id' => $file->id(),
        ],
      ]);

      // Save the media entity.
      $media->save();

      // Delete local file
      $this->fileSystem->delete($destination);

      $media_id = $media->id();
      $media_uuid = $media->uuid();

      /**
       * Insert Media ID to table
       * Create event media load and change URL
       */

      $insertData = [
        'uuid' => $data['uuid'],
        'url' => $data['url'],
        'filename' => $data['frFilename'],
        'media_id' => $media_id,
        'local_uuid' => $media_uuid
      ];
      $this->database->insert("filerobot_medias")->fields($insertData)->execute();
    } else {
      $media_id = 0;
      $media_uuid = '';
      foreach ($result as $row) {
        $media_id = $row->media_id;
        $media_uuid = $row->local_uuid;
      }
    }

    return new JsonResponse(
      [
        'message' => 'Image inserted successfully.',
        'media_id' => $media_id,
        'media_uuid' => $media_uuid
      ]
    );
  }
}
