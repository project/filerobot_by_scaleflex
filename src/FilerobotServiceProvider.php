<?php

namespace Drupal\filerobot;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\filerobot\Plugin\MediaLibrary\UiBuilder\FilerobotMediaLibraryUiBuilder;

class FilerobotServiceProvider extends ServiceProviderBase implements ServiceProviderInterface {

  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('media_library.ui_builder');
    $definition->setClass(FilerobotMediaLibraryUiBuilder::class);
  }
}
