# Drupal Filerobot Module

## Download and Installation Filerobot module

### Installation from Github
- Step 1: Download the latest version [Download the latest release of the Module](https://www.drupal.org/project/filerobot_by_scaleflex/releases)
- Step 2: Extract the zip file at location "**/modules**"
- Step 3: In Admin go to Extend -&gt; find Filerobot by Scaleflex -&gt; Install

### Or Installation with Packagist
- Step 1: Run "**composer require drupal/filerobot_by_scaleflex:^1.0**"
- Step 2: In Admin go to Extend -&gt; find Filerobot by Scaleflex -&gt; Install
####
![](docs/install-module.png)

## Configuration
![](docs/configuration-module.png)
- **Activation**: Enable/Disable the module
- **Token**: Please enter your Filerobot token here (eg: abcdefgh)
- **CNAME**: Enter the CNAME as per the configuration done in your Filerobot Asset Hub interface, once validated and the SSL certificate is accepted. (Or leave blank if none).
- **Security Template Identifier:** To load the Filerobot Widget or Filerobot Image Editor, you need to create a Security Template in your Filerobot Asset Hub first, in order for your Drupal instantiation of the Filerobot Widget to obtain proper credentials and access your storage.
- **Filerobot upload directory:** The directory in your Filerobot account, where the files will be stored.

## Action needed when update "Media Image"
You need to click the button "Update widget" to load the image after choosing the image from Filerobot widget.
![](docs/img-1.png)
![](docs/img-2.png)
