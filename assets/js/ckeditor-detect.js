let activeCKEditor = null;
jQuery(document).ready(function ($){
  let CKEditor5Instances = Drupal.CKEditor5Instances;
  if (CKEditor5Instances !== undefined) {
    for (const item of CKEditor5Instances.keys()) {
      $('textarea[data-ckeditor5-id="' + item + '"]').next()
        .find('button[data-cke-tooltip-text="Insert Media"]')
        .attr('onclick', 'detectEditor("' + item + '")');
    }
  }
});

function detectEditor(item) {
  activeCKEditor = item;
}
