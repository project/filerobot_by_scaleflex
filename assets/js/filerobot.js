jQuery(document).ready(function ($){
  $(document).on({
    ajaxSuccess: function (event, xhr, settings) {
      let response = JSON.parse(xhr.responseText);
      for (let i = 0; i < response.length; i++) {
        if (response[i].command !== undefined && response[i].command === 'openDialog') {
          if (response[i].data.includes('filerobot-widget')) {
            setTimeout(renderWidget, 1000);
          }
        }
      }
    }
  });

  let renderFilerobot = setInterval(renderWidget, 1000);

  function renderWidget() {
    (function ($) {
      if (document.getElementById("filerobot-widget") !== null) {
        const filerobotWidget = document.getElementById("filerobot-widget");
        clearTimeout(renderFilerobot);
        let Filerobot = window.Filerobot;

        let filerobot = null;
        // Locale
        filerobot = Filerobot.Core({
          securityTemplateID: frSettings.SEC,
          container: frSettings.token,
        });

        // Plugins
        let Explorer = Filerobot.Explorer;
        let XHRUpload = Filerobot.XHRUpload;

        // Optional plugins:
        let ImageEditor = Filerobot.ImageEditor;
        // let Webcam = Filerobot.Webcam;

        filerobot
          .use(Explorer, {
            config: {
              rootFolderPath: frSettings.uploadDirectory
            },
            target: '#filerobot-widget',
            inline: true,
            width: "100%",
            height: "100%",
            disableExportButton: false,
            hideExportButtonIcon: true,
            preventExportDefaultBehavior: true,
            dismissUrlPathQueryUpdate: true,
            disableDownloadButton: false,
            hideDownloadButtonIcon: true,
            preventDownloadDefaultBehavior: true,
            locale: {
              strings: {
                mutualizedExportButtonLabel: 'Export',
                mutualizedDownloadButton: 'Export'
              }
            },
          })
          .use(XHRUpload)
          .use(ImageEditor)
          .on('export', async (files, popupExportSuccessMsgFn, downloadFilesPackagedFn, downloadFileFn) => {
            // console.dir(files);
            let frFooterButton = document.getElementsByClassName("SfxButton-root");
            for (let i = 0; i < frFooterButton.length; i++) {
              frFooterButton[i].setAttribute('disabled', 'true');
            }

            let textProcessing = 'Processing';
            let frExportButton = document.getElementsByClassName('filerobot-Explorer-TopBar-DownloadWithExportButton-downloadButton');
            let textExport;
            for (let i = 0; i < frExportButton.length; i++) {
              textExport = frExportButton[i].innerHTML;
              frExportButton[i].setAttribute('disabled', 'true');
              frExportButton[i].innerHTML = textProcessing;
            }

            for (const selected of files) {
              let filerobotURL = selected.file.url.cdn;
              let newFilerobotUrl = new URL(filerobotURL);
              if (newFilerobotUrl.searchParams.has('vh')) {
                newFilerobotUrl.searchParams.delete('vh');
              }

              if (frSettings.CNAME !== '' && frSettings.CNAME !== null) {
                newFilerobotUrl.host = frSettings.CNAME;
                newFilerobotUrl.hostname = frSettings.CNAME;
              }

              fetch('/api/filerobot-insert-image', {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                  'X-CSRF-Token': Drupal.csrfToken
                },
                body: JSON.stringify({
                  "uuid": selected.file.uuid,
                  "url": newFilerobotUrl.href,
                  "frFilename": selected.file.name
                })
              })
                .then(response => {
                  if (!response.ok) {
                    throw new Error(response.statusText);
                  }
                  return response.json();
                })
                .then(data => {
                  let media_id = data.media_id;
                  let media_uuid = data.media_uuid;
                  if (!$('#filerobot-widget').hasClass('open-editor')) {
                    $('input[data-media-library-widget-value="field_media_image"]').val(media_id);
                    $('input[data-media-library-widget-update="field_media_image"]').show();
                    $('input[data-media-library-widget-update="field_media_image"]').attr('onclick', 'updateWidget()');
                    $('.ui-dialog-titlebar-close').trigger('click');
                  } else {
                    let insertImg = `<drupal-media data-entity-type="media" data-entity-uuid="${media_uuid}" data-align="center">&nbsp;</drupal-media>`;
                    Drupal.CKEditor5Instances.get(activeCKEditor).model.change((writer) => {
                      const insertPosition = Drupal.CKEditor5Instances.get(activeCKEditor).model.document.selection.getFirstPosition();
                      writer.insert(insertImg, insertPosition);
                      let newData = Drupal.CKEditor5Instances.get(activeCKEditor).getData();
                      newData = newData.replaceAll('&lt;', '<');
                      newData = newData.replaceAll('&gt;', '>');
                      Drupal.CKEditor5Instances.get(activeCKEditor).setData(newData);
                    });
                    $('.ui-dialog-titlebar-close').trigger('click');
                    activeCKEditor = null;
                  }
                })
                .catch(error => {
                  console.error(error);
                });
            }
          })
          .on('complete', ({failed, uploadID, successful}) => {
            if (failed) {
              console.dir(failed);
            }

            if (successful) {
              // console.dir(successful);
              successful.forEach((item, key) => {
                // do something
              });
            }
          });
      }
    })(jQuery);
  }
});

function updateWidget()
{
  jQuery('input[data-media-library-widget-update="field_media_image"]').hide();
}
